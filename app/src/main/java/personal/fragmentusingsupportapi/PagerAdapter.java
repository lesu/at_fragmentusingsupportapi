package personal.fragmentusingsupportapi;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

class PagerAdapter extends FragmentPagerAdapter{
    private Fragment[] fragments;

    public PagerAdapter(FragmentManager fm,Fragment... fragments) {
        super(fm);
        this.fragments = fragments;
    }


    @Override
    public Fragment getItem(int position) {
        return this.fragments[position];
    }


    @Override
    public int getCount() {
        return fragments.length;
    }
}
