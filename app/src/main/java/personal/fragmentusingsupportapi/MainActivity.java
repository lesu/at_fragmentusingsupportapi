package personal.fragmentusingsupportapi;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * <a href="http://stackoverflow.com/a/21478192/5132301">stackoverflow.com | ViewPager in inner fragment, not showing up, not visible</a>
 * ViewPager inside a ScrollView will not work.
 */
public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }


    private void init() {
        this.viewPager = (ViewPager) this.findViewById(R.id.main_support_view_pager);
        this.viewPager.setAdapter(new PagerAdapter(this.getSupportFragmentManager(),new Tab1(),new Tab2()));
        this.viewPager.setCurrentItem(0);
    }
}
